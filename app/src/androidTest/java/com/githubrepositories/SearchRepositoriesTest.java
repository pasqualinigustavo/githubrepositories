package com.githubrepositories;

import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.githubrepositories.activities.LauncherActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class SearchRepositoriesTest {

    @Rule
    public ActivityTestRule<LauncherActivity> menuActivityTestRule = new ActivityTestRule<>(LauncherActivity.class);

    /*
    Teste para verificar se consegue conectar, baixar repositórios e abrir um deles.
     */
    @Test
    public void repositories() {
        pauseTestFor(1000);
        //open drawer menu
        onView(withContentDescription(R.string.label_open))
                .perform(click());

        //Open option HOME (it' the default fragment, but only to know the download of repositories)
        onView(withText(R.string.label_item_home))
                .perform(click());

        pauseTestFor(500);

        //click in one item to show pull requests
        onView(withId(R.id.fragment_repositories_listview)).perform(click());

        pauseTestFor(1000);
        //OK!
    }

    private void pauseTestFor(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
