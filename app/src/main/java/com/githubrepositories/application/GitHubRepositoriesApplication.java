package com.githubrepositories.application;

import android.content.Context;
import android.support.graphics.drawable.BuildConfig;

import com.githubrepositories.utils.SharedPreferencesUtils;

public class GitHubRepositoriesApplication extends AbstractApplication {

    private static Context context = null;
    public static String TAG = GitHubRepositoriesApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG) {
            //for example turn on crashlitycs Fabric
        } else {
            //for example turn off crashlitycs Fabric
        }

        //Global context of app
        GitHubRepositoriesApplication.context = getApplicationContext();
        SharedPreferencesUtils.init(getApplicationContext());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static Context getAppContext() {
        return GitHubRepositoriesApplication.context;
    }

}
