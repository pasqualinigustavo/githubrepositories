package com.githubrepositories.application;

import android.app.Application;
import android.util.Log;

public abstract class AbstractApplication extends Application {
    protected final String TAG;
    private AppContext appContext = null;

    public AbstractApplication() {
        super();
        this.TAG = this.getClass().getSimpleName();

        appContext = AppContext.getInstance(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

        switch (level) {
            case TRIM_MEMORY_COMPLETE:
                this.tryToCleanMemory();
                break;

            case TRIM_MEMORY_UI_HIDDEN:
                this.tryToCleanMemory();
                break;

            case TRIM_MEMORY_RUNNING_CRITICAL:
                this.cleanMemoryCache();
                break;
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.d(TAG, "onLowMemory");
        this.cleanMemoryCache();
    }

    private void tryToCleanMemory() {
        this.cleanMemoryCache();
        System.gc();
    }

    private void cleanMemoryCache() {
        //TODO: Clean memory
    }
}
