package com.githubrepositories.application;

import android.content.Context;

import com.githubrepositories.R;

public class AppContext {

    private static AppContext instance;
    private Context context = null;

    private AppContext(Context context) {
        this.context = context;
    }

    public static synchronized AppContext getInstance(Context context) {
        if (instance == null) {
            instance = new AppContext(context);
        }
        return instance;
    }

    public boolean isTablet() {
        boolean tabletSize = context.getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            return true;
        } else {
            return false;
        }
    }


    public Context getContext() {
        return context;
    }
}
