package com.githubrepositories.components.imageview;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.githubrepositories.utils.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.security.NoSuchAlgorithmException;

public class ImageFileController {
    private static final String DIR_IMAGES = "images";
    public static final String DIR_TEMP = "temp";

    private final Context mContext;

    public ImageFileController(Context context) {
        this.mContext = context;
    }

    public void writeImageOnDisk(String uri, Bitmap loadedImage, String imagePath) throws Exception {
        writeImageOnDisk(uri, loadedImage, imagePath, null);
    }

    public File writeImageOnDisk(String imageUri, Bitmap loadedImage, String imagePath, String mLocalFileName) throws Exception {
        Bitmap img = loadedImage;

        // /data/data/com.package/app_images/{imageDir}/{MD5}.png
        if (StringUtils.isNullOrEmpty(imagePath)) {
            imagePath = DIR_TEMP.concat("/").concat(mLocalFileName);
        }
        File imageDir = getDir(imagePath);

        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }

        String fileName;
        if (!StringUtils.isNullOrEmpty(mLocalFileName)) {
            fileName = mLocalFileName;
        } else {
            fileName = getImageName(imageUri);
        }
        File catalogoFile = new File(imageDir, fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(catalogoFile);
            img.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            throw e;
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
        return catalogoFile;
    }

    public File getDir(String imageDir) {
        File catalogoDir = new File(getImageDir(), imageDir);
        if (!catalogoDir.isDirectory())
            catalogoDir.mkdir();

        return catalogoDir;
    }

    public File getImageDir() {
        ContextWrapper cw = new ContextWrapper(mContext);
        File directory = cw.getDir(DIR_IMAGES, Context.MODE_PRIVATE);

        return directory;
    }

    public static String getImageName(String uri) throws NoSuchAlgorithmException {
        return Md5Utils.MD5(uri);
    }

    /**
     * Verifica se possui imagem no diretório informado
     *
     * @return
     */
    public Bitmap getImage(String md5FileName, String imagePath) {
        Bitmap img = null;

        File path = getDir(imagePath);
        File imageFile = new File(path, md5FileName);
        if (imageFile.exists()) {
            img = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        }
        return img;
    }

    public File getImageFile(String md5FileName, String imagePath) {
        if (!StringUtils.isNullOrEmpty(imagePath) && !StringUtils.isNullOrEmpty(md5FileName)) {
            File path = getDir(imagePath);
            File imageFile = new File(path, md5FileName);
            if (imageFile.exists()) {
                return imageFile;
            }
        }
        return null;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
