package com.githubrepositories.utils;

/**
 * Created by pasqualini on 10/27/15.
 */
public interface GitHubRepositoriesConstants {

    // NOTIFICATION
    String NOTIFICATION_CENTER_PHOTOBOOK = "NOTIFICATION_CENTER_PHOTOBOOK";
    String NOTIFY_CLOSE_MENU = "NOTIFY_CLOSE_MENU";
    String NOTIFY_LOGOFF = "NOTIFY_LOGOFF";

    // Aptoide Requests
    public static String API_URL_APPS_LIST = "http://ws2.aptoide.com/api/6/bulkRequest/api_list/listApps";
    public static String API_URL_APP_BY_ID = "http://ws2.aptoide.com/api/7/getApp/app_id/";

    //RecyclerView
    public static int LIST_ITEM_ITEMS_PER_PAGE = 20;

    // General definitions
    public static final int HTTP_CONNECTION_TIMEOUT = 60000;
}
