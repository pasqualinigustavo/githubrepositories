package com.githubrepositories.utils;

import android.app.Activity;
import android.view.View;

import com.githubrepositories.R;

/**
 * Created by pasqualini on 18/06/2015.
 */
public class LoadingUtils {

    public static void show(final Activity activity) {
        if (activity == null) {
            return;
        }

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View view = activity.findViewById(R.id.loading_layout);
                if (view != null) {
                    view.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public static void hide(final Activity activity) {
        if (activity == null) {
            return;
        }

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View view = activity.findViewById(R.id.loading_layout);
                if(view!=null)
                {
                   view.setVisibility(View.GONE);
                }
            }
        });
    }
}
