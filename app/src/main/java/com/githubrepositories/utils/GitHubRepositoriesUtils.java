package com.githubrepositories.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.URLUtil;

public class GitHubRepositoriesUtils {

    public static void openUrlWithoutConfirmation(Context context, String link) {
        try {
            if (!StringUtils.isNullOrEmpty(link) && URLUtil.isValidUrl(link)) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(link));
                context.startActivity(i);
            }

        } catch (Exception e) {
            Log.e("Exception", String.valueOf(e));
        }
    }
}
