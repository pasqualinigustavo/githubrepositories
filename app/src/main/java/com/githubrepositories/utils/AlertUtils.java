package com.githubrepositories.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;

import com.githubrepositories.R;

/**
 * Created by pasqualini on 19/11/15.
 */
public class AlertUtils {

    public static void showMessage(final Context context, @StringRes int title, @StringRes int message) {
        AlertUtils.showMessage(context, context.getString(title), context.getString(message));
    }

    public static void showMessage(final Context context, String title, String message) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);
        alertBuilder.setPositiveButton(context.getString(R.string.label_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alertBuilder.create();
        dialog.show();
    }
}
