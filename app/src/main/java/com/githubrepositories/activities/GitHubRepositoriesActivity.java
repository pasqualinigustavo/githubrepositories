package com.githubrepositories.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.githubrepositories.R;
import com.githubrepositories.activities.interfaces.ActivityToolbarBehaviour;
import com.githubrepositories.activities.interfaces.MenuBehaviour;
import com.githubrepositories.application.AppContext;
import com.githubrepositories.fragment.GitHubRepositoriesMenu;
import com.githubrepositories.fragment.RepositoriesFragment;
import com.githubrepositories.model.base.AbstractActivity;
import com.githubrepositories.model.base.AbstractFragment;
import com.githubrepositories.model.base.BasicItem;
import com.githubrepositories.utils.GitHubRepositoriesConstants;

public class GitHubRepositoriesActivity extends AbstractActivity implements MenuBehaviour, ActivityToolbarBehaviour, FragmentManager.OnBackStackChangedListener {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private final Handler drawerHandler = new Handler();

    private Toolbar toolbar;
    private ImageView imageToolbar;
    private TextView labelTitleToolbar;
    private AppContext appContext = null;

    public GitHubRepositoriesActivity() {
        super(R.layout.activity_main, R.id.activity_content);
        this.useTransitionAnimation = true;
        this.transitionAnimationEnter = R.anim.fade_in;
        this.transitionAnimationExit = R.anim.fade_out;
        this.transitionAnimationPopEnter = R.anim.fade_in;
        this.transitionAnimationPopExit = R.anim.fade_out;
    }

    @Override
    protected void initComponents() {
        this.drawerLayout = (DrawerLayout) findViewById(R.id.activity_drawer);

        this.imageToolbar = (ImageView) findViewById(R.id.actionbar_logo);
        this.labelTitleToolbar = (TextView) findViewById(R.id.actionbar_title);

        this.createActionBar();
        appContext = AppContext.getInstance(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void clearBackStackWhenActivityWasKilled() {
        if (this.hasBackStack()) {
            AbstractFragment.FragmentUtils.sDisableFragmentAnimations = true;
            getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            AbstractFragment.FragmentUtils.sDisableFragmentAnimations = false;
        }
    }

    @Override
    protected void initFragments(Bundle savedInstanceState) {
        this.switchContent(new GitHubRepositoriesMenu(), R.id.activity_menu, false);

        if(savedInstanceState == null) {
            this.clearBackStackWhenActivityWasKilled();
            this.switchContent(new RepositoriesFragment(), this.viewContentId, false);
        }
    }

    @Override
    protected void initData() {


    }

    @Override
    protected void initListeners() {
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasBackStack()) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    toggleMenu();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (this.drawerLayout.isDrawerOpen(Gravity.START)) {
            this.closeMenu();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onBackStackChanged() {
        this.displayHomeUp();
        this.hideKeyboard();
    }

    @Override
    public void openContentMenu(BasicItem basicItem) {
        this.scheduleLaunchAndCloseDrawer(basicItem);
    }

    private void scheduleLaunchAndCloseDrawer(final BasicItem basicItem) {
        this.drawerHandler.removeCallbacksAndMessages(null);
        this.drawerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // disable transition bettewen fragment when change by menu, that made a strange effect
                AbstractFragment.FragmentUtils.sDisableFragmentAnimations = true;
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                actionOpenFragment(basicItem);
                getSupportFragmentManager().executePendingTransactions();
                AbstractFragment.FragmentUtils.sDisableFragmentAnimations = false;
            }
        }, 300);

        this.closeMenu();
    }

    @Override
    public void closeMenu() {
        if (this.drawerLayout.isDrawerOpen(Gravity.START)) {
            this.drawerLayout.closeDrawer(Gravity.START);
        }
    }

    @Override
    public void openMenu() {
        if (!this.drawerLayout.isDrawerOpen(Gravity.START)) {
            this.drawerLayout.openDrawer(Gravity.START);
        }
    }

    @Override
    public void toggleMenu() {
        if (this.drawerLayout.isDrawerOpen(Gravity.START)) {
            this.drawerLayout.closeDrawer(Gravity.START);
//            GoogleAnalyticsUtils.getInstance().sendEvent(this, "MENU", "CLOSED", null, 0);
        } else {
            this.drawerLayout.openDrawer(Gravity.START);
//            GoogleAnalyticsUtils.getInstance().sendEvent(this, "MENU", "OPENED", null, 0);
        }

        this.hideKeyboard();
    }

    @Override
    public void setToolbarTitle(String title) {
        if (this.toolbar == null) {
            return;
        }
        this.labelTitleToolbar.setText(title);
        this.labelTitleToolbar.setVisibility(View.VISIBLE);
        this.imageToolbar.setVisibility(View.GONE);

        this.displayHomeUp();
    }

    @Override
    public void setToolbarTitle(int imageResId) {
        if (this.toolbar == null) {
            return;
        }
        this.imageToolbar.setImageResource(imageResId);
        this.imageToolbar.setVisibility(View.VISIBLE);
        this.labelTitleToolbar.setVisibility(View.GONE);

        this.displayHomeUp();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.drawerToggle.onConfigurationChanged(newConfig);
//        if(!appContext.isTablet())
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
    }

    private void createActionBar() {
        this.getSupportFragmentManager().addOnBackStackChangedListener(this);

        this.toolbar = (Toolbar) findViewById(R.id.activity_actionbar);
        this.setSupportActionBar(this.toolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDisplayUseLogoEnabled(true);

        this.drawerToggle = new ActionBarDrawerToggle(this, this.drawerLayout, this.toolbar, R.string.label_open, R.string.label_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                actionNotifyCloseMenu();
                hideKeyboard();
            }
        };

        this.drawerLayout.setDrawerListener(this.drawerToggle);
    }

    private void actionNotifyCloseMenu() {
        Intent intent = new Intent(GitHubRepositoriesConstants.NOTIFICATION_CENTER_PHOTOBOOK);
        intent.putExtra(GitHubRepositoriesConstants.NOTIFY_CLOSE_MENU, true);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void actionOpenFragment(BasicItem basicItem) {
        this.switchContent(basicItem.getFragmentClass(), true);
    }

    public void displayHomeUp() {
        boolean isRoot = !this.hasBackStack();
        this.drawerToggle.setDrawerIndicatorEnabled(isRoot);

        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setDisplayShowHomeEnabled(!isRoot);
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(!isRoot);
            this.getSupportActionBar().setHomeButtonEnabled(!isRoot);
        }

        if (isRoot) {
            this.drawerToggle.syncState();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }
}


