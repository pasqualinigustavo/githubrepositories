package com.githubrepositories.activities.interfaces;

import com.githubrepositories.model.base.BasicItem;

public interface MenuBehaviour {
    void openContentMenu(BasicItem basicItem);
    void closeMenu();
    void openMenu();
}
