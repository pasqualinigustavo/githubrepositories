package com.githubrepositories.activities.interfaces;

/**
 * Created by pasqualini 18/06/2015.
 */
public interface ActivityToolbarBehaviour {
    void toggleMenu();
    void setToolbarTitle(String title);
    void setToolbarTitle(int imageResId);
}
