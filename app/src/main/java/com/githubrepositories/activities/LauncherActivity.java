package com.githubrepositories.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.githubrepositories.R;
import com.githubrepositories.application.AppContext;
import com.githubrepositories.utils.StringUtils;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class LauncherActivity extends AppCompatActivity {

    private boolean finished = false;
    private Date timeStart;
    private AppContext appContext = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_launcher);

        appContext = AppContext.getInstance(this);

        this.timeStart = new Date();
        this.initData();
    }

    private void initData() {
        openInflux();
    }

    private void handleError(final String message, boolean thereAreInitData) {
        if (thereAreInitData) {
            this.openInflux();
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (StringUtils.hasValue(message)) {
                    Log.e(LauncherActivity.class.getSimpleName(), message);
                    Toast.makeText(LauncherActivity.this, message, Toast.LENGTH_SHORT).show();
                }

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(LauncherActivity.this);
                alertBuilder.setTitle(R.string.label_attention);
                alertBuilder.setMessage(R.string.message_launcher_problem_to_download);
                alertBuilder.setPositiveButton(R.string.label_launcher_try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        initData();
                        dialog.dismiss();
                    }
                });
                alertBuilder.setCancelable(false);
                alertBuilder.show();
            }
        });
    }

    private void openInflux() {
        if (!this.finished) {
            finished = true;

            Date timeEnded = new Date();
            long diff = timeEnded.getTime() - this.timeStart.getTime();
            long diffLoader = 0 - diff;

            if (diffLoader <= 0) {
                diffLoader = 1;
            }

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    actionOpenActivity();
                }
            }, diffLoader);
        }
    }

    private void actionOpenActivity() {
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        this.startActivity(new Intent(this, GitHubRepositoriesActivity.class));
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
