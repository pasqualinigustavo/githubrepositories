package com.githubrepositories.activities;

import com.githubrepositories.model.base.AbstractActivity;

public abstract class BaseActivity extends AbstractActivity {


    public BaseActivity(int layoutId, int viewContentId) {
        super(layoutId, viewContentId);
    }

    public BaseActivity(int layoutId) {
        super(layoutId);
    }
}
