package com.githubrepositories.rest;

import retrofit.RequestInterceptor;

/**
 * Created by pasqualini.
 */
public class RestAdapter {

    public static String URL_BASE = "https://api.github.com";

    static RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestInterceptor.RequestFacade request) {

        }
    };


    public static retrofit.RestAdapter getCore() {
        return new retrofit.RestAdapter.Builder()
                .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
                .setEndpoint(URL_BASE)
                .setRequestInterceptor(RestAdapter.requestInterceptor)
                .build();
    }
}
