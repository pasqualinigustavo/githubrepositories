package com.githubrepositories.rest.endpoint;

import com.githubrepositories.model.PullRequest;
import com.githubrepositories.rest.response.RepositoriesResponse;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by pasqualini
 */
public interface SearchEndPoint {

    @GET("/search/repositories?q=language:Java&sort=stars")
    void getRepositories(@Query("page") int page,
                         Callback<RepositoriesResponse> response);

    @GET("/repos/{owner}/{repository}/pulls")
    void getRepositoryPullRequests(@Path("owner") String owner,
                                   @Path("repository") String repository,
                                   @Query("page") int page,
                                   Callback<List<PullRequest>> response);


}
