
package com.githubrepositories.rest.response;

import com.githubrepositories.model.Item;

import java.util.ArrayList;
import java.util.List;

public class RepositoriesResponse {

    public int total_count = 0;
    public boolean incomplete_results = false;
    public List<Item> items = new ArrayList<Item>();

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
