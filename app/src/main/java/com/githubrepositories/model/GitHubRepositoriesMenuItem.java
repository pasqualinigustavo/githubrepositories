package com.githubrepositories.model;

import android.os.Bundle;

import com.githubrepositories.model.base.AbstractFragment;
import com.githubrepositories.model.base.BasicItem;
import com.githubrepositories.model.interfaces.DefaultInfoList;

public class GitHubRepositoriesMenuItem extends BasicItem implements DefaultInfoList {

    private boolean favorite;
    private boolean selected = true;

    public GitHubRepositoriesMenuItem() {
        super();
    }

    public GitHubRepositoriesMenuItem(String name, int imageId) {
        super(name, imageId);
    }

    public GitHubRepositoriesMenuItem(String name, int imageId, Class<? extends AbstractFragment> fragmentClass) {
        super(name, imageId, fragmentClass);
    }

    public GitHubRepositoriesMenuItem(String name, int imageId, Class<? extends AbstractFragment> fragmentClass, int totalUpdates) {
        super(name, imageId, fragmentClass);
    }

    public GitHubRepositoriesMenuItem(String name, int imageId, Class<? extends AbstractFragment> fragmentClass, String valueStr, int totalUpdates) {
        super(name, imageId, fragmentClass, valueStr);
    }

    public GitHubRepositoriesMenuItem(String name, int imageId, Class<? extends AbstractFragment> fragmentClass, long valueLong, int totalUpdates) {
        super(name, imageId, fragmentClass, valueLong);
    }

    public GitHubRepositoriesMenuItem(String name, int imageId, Class<? extends AbstractFragment> fragmentClass, Bundle bundle, int totalUpdates) {
        super(name, imageId, fragmentClass, bundle);
    }

    @Override
    public int getIconId() {
        return this.getImageId();
    }

    @Override
    public String getIconUrl() {
        return null;
    }

    @Override
    public String getTitle() {
        return this.getName();
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
