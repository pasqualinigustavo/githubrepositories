package com.githubrepositories.model.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import com.githubrepositories.model.interfaces.ActivityBehaviour;

public abstract class AbstractFragment extends Fragment {

    protected final String TAG;

    public static class FragmentUtils {
        public static boolean sDisableFragmentAnimations = false;
    }

    public static final String VALUE_STR = "VALUE_STR";
    public static final String VALUE_LONG = "VALUE_LONG";

    protected ActivityBehaviour activityBehaviour;
    protected String name;
    protected int layoutId;

    public AbstractFragment() {
        super();
        this.TAG = this.getClass().getSimpleName();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(this.layoutId, container, false);

        if (getActivity() instanceof ActivityBehaviour) {
            this.activityBehaviour = (ActivityBehaviour) getActivity();
        }

        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.init();
    }

    protected void init() {
        this.initComponent(this.getView());
        this.initData();
        this.initListeners();
    }

    protected abstract void initComponent(final View view);

    protected abstract void initListeners();

    protected abstract void initData();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {
            };
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    /**
     * @param fragment
     */
    protected void switchContent(final AbstractFragment fragment) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContent(fragment);
        }
    }

    /**
     * @param fragment
     */
    protected void switchContent(final AbstractFragment fragment, final boolean addToBackStack) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContent(fragment, addToBackStack);
        }
    }

    protected void switchContent(final Class<? extends AbstractFragment> fragmentClass) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContent(fragmentClass);
        }
    }

    protected void switchContent(final AbstractFragment fragment, int idRes, boolean animation)
    {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContent(fragment, idRes,animation);
        }
    }

    protected void switchContent(final AbstractFragment fragment, int idRes, boolean addToBackStack, boolean animation)
    {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContent(fragment, idRes, addToBackStack, animation);
        }
    }

    protected void switchContent(final Class<? extends AbstractFragment> fragmentClass, final boolean addToBackStack) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContent(fragmentClass, addToBackStack);
        }
    }

    protected void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String valueStr, final boolean addToBackStack) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContent(fragmentClass, null, valueStr, addToBackStack);
        }
    }

    protected void switchContent(final Class<? extends AbstractFragment> fragmentClass, final long valueLong, final boolean addToBackStack) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContent(fragmentClass, null, valueLong, addToBackStack);
        }
    }

    protected void switchContentDelay(final Class<? extends AbstractFragment> fragmentClass, int delay, final boolean animate, final boolean cleanStack, final boolean addToBackStack) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContentDelay(fragmentClass, delay, animate, cleanStack, addToBackStack);
        }
    }

    protected void switchContentDelay(final AbstractFragment fragment, int delay, final boolean animate, final boolean cleanStack, final boolean addToBackStack) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.switchContentDelay(fragment, delay, animate, cleanStack, addToBackStack);
        }
    }

    protected void clearFragmentStack() {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.clearFragmentStack();
        }
    }

    protected void backFragment() {
        this.getActivity().onBackPressed();
    }

    protected void activityEffectTransition() {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.activityEffectTransition();
        }
    }

    protected void activityEffectTransition(int enterAnim, int exitAnim) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.activityEffectTransition(enterAnim, exitAnim);
        }
    }

    protected void blockOnBackPress(final boolean block) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.blockOnBackPress(block);
        }
    }

    protected void hideKeyboard() {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.hideKeyboard();
        }
    }

    protected void showKeyboard() {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.showKeyboard();
        }
    }

    protected void executeIntent(final Intent target, @StringRes int title, @StringRes int messageProblem) {
        if (this.activityBehaviour != null) {
            this.activityBehaviour.executeIntent(target, title, messageProblem);
        }
    }

}

