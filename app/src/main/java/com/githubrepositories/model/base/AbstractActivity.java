package com.githubrepositories.model.base;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.githubrepositories.R;
import com.githubrepositories.model.interfaces.ActivityBehaviour;
import com.githubrepositories.utils.StringUtils;

import java.util.Date;

public abstract class AbstractActivity extends AppCompatActivity implements ActivityBehaviour {
    protected final String TAG;

    protected int layoutId;
    protected int viewContentId;

    private final Handler switchContentHandler = new Handler();

    private static final int NONE = 0;
    protected boolean useTransitionAnimation = false;
    protected int transitionAnimationEnter = NONE;
    protected int transitionAnimationExit = NONE;
    protected int transitionAnimationPopEnter = NONE;
    protected int transitionAnimationPopExit = NONE;

    private Date tryToClose = null;
    private boolean blockOnBackPress = false;

    private InputMethodManager inputManager;

    /**
     * Message before close the activity
     */
    protected boolean questionBeforeClose = true;

    /**
     * Default constructor, need to pass the layoutId and contentId
     *
     * @param layoutId
     * @param viewContentId
     */
    public AbstractActivity(int layoutId, int viewContentId) {
        super();
        this.TAG = this.getClass().getSimpleName();

        this.layoutId = layoutId;
        this.viewContentId = viewContentId;
    }

    public AbstractActivity(int layoutId) {
        super();
        this.TAG = this.getClass().getSimpleName();

        this.layoutId = layoutId;
        this.viewContentId = 0;
    }

    /**
     * Method responsible to start the activity and define the behavior of the activity
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(this.layoutId);

        // The Theme's windowBackground is masked by the opaque background of the activity, and the windowBackground causes an
        // uncecessarry overdraw. Nulllfying the windowBackground removes that background
        this.getWindow().setBackgroundDrawable(null);

        this.init(savedInstanceState);
        this.setConfiguration();
    }

    /**
     * Method responsible to call all init method.
     * If you need to change the sequence/behavior of all init call override this method
     */
    protected void init(Bundle savedInstanceState) {
        this.blockOnBackPress = false;

        this.initComponents();
        this.initFragments(savedInstanceState);
        this.initListeners();
        this.initData();
    }

    /**
     * Set up configurations
     */
    protected void setConfiguration() {
        if (this.useTransitionAnimation &&
                (this.transitionAnimationEnter == NONE ||
                        this.transitionAnimationExit == NONE ||
                        this.transitionAnimationPopEnter == NONE ||
                        this.transitionAnimationPopExit == NONE)) {
            this.transitionAnimationEnter = R.anim.slide_in_from_right;
            this.transitionAnimationExit = R.anim.slide_out_to_left;
            this.transitionAnimationPopEnter = R.anim.slide_in_from_left;
            this.transitionAnimationPopExit = R.anim.slide_out_to_right;
        }
    }

    /**
     * Method onBackPressed was override putting the default behavior to the activity in doesn't
     * close immediately, given the the opportunity to change their mind
     */
    @Override
    public void onBackPressed() {

        if (this.blockOnBackPress) {
            return;
        }

        if (!questionBeforeClose) {
            super.onBackPressed();
            return;
        }

        if (this.hasBackStack()) {
            super.onBackPressed();
            return;
        }

        if (this.tryToClose != null) {
            Date tryToCloseAgain = new Date();
            long diff = tryToCloseAgain.getTime() - tryToClose.getTime();
            long diffSec = diff / 1000;

            if (diffSec > 1) {
                this.tryToClose = null;
            } else {
                super.onBackPressed();
                return;
            }
        }

        this.tryToClose = new Date();
        Toast.makeText(this, getString(R.string.message_close_app), Toast.LENGTH_SHORT).show();
    }

    protected abstract void initComponents();

    protected abstract void initFragments(Bundle savedInstanceState);

    protected abstract void initData();

    protected abstract void initListeners();

    @Override
    public void switchContent(final AbstractFragment fragment) {
        setOrientation();
        this.switchContent(fragment, this.viewContentId, this.useTransitionAnimation);
    }

    @Override
    public void switchContent(final AbstractFragment fragment, boolean addBackStack) {
        setOrientation();
        this.switchContent(fragment, this.viewContentId, addBackStack, this.useTransitionAnimation);
    }

    @Override
    public void switchContent(final AbstractFragment fragment, int idRes, boolean animation) {
        setOrientation();
        this.switchContent(fragment, idRes, false, animation);
    }

    @Override
    public void switchContent(final AbstractFragment fragment, int idRes, boolean addToBackStack, boolean animation) {
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        setOrientation();
        if (this.useTransitionAnimation && animation) {
            transaction.setCustomAnimations(
                    this.transitionAnimationEnter, this.transitionAnimationExit,
                    this.transitionAnimationPopEnter, this.transitionAnimationPopExit);
        }

        String tag = fragment.getClass().getCanonicalName();
        transaction.replace(idRes, fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    @Override
    public void switchContent(final Class<? extends AbstractFragment> fragmentClass) {
        setOrientation();
        this.switchContent(fragmentClass, null, false);
    }

    @Override
    public void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name) {
        setOrientation();
        this.switchContent(fragmentClass, name, false);
    }

    @Override
    public void switchContent(final Class<? extends AbstractFragment> fragmentClass, final boolean addToBackStack) {
        setOrientation();
        this.switchContent(fragmentClass, null, addToBackStack);
    }

    @Override
    public void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name, final boolean addToBackStack) {
        setOrientation();
        this.switchContent(fragmentClass, name, addToBackStack, null);
    }

    @Override
    public void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name, final String valueStr, final boolean addToBackStack) {
        setOrientation();
        Bundle bundle = new Bundle();
        bundle.putString(AbstractFragment.VALUE_STR, valueStr);
        this.switchContent(fragmentClass, name, addToBackStack, bundle);
    }

    @Override
    public void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name, final Long valueLong, final boolean addToBackStack) {
        setOrientation();
        Bundle bundle = new Bundle();
        bundle.putLong(AbstractFragment.VALUE_LONG, valueLong);
        this.switchContent(fragmentClass, name, addToBackStack, bundle);
    }

    @Override
    public void switchContent(final BasicItem basicItem) {
        setOrientation();
        this.switchContent(basicItem, false);
    }

    @Override
    public void switchContent(final BasicItem basicItem, final boolean addToBackStack) {
        setOrientation();
        if (basicItem.getBundle() != null) {
            this.switchContent(basicItem.getFragmentClass(), basicItem.getName(), addToBackStack, basicItem.getBundle());

        } else if (StringUtils.hasValue(basicItem.getValueStr())) {
            this.switchContent(basicItem.getFragmentClass(), basicItem.getName(), basicItem.getValueStr(), addToBackStack);

        } else if (basicItem.getValueLong() != null) {
            this.switchContent(basicItem.getFragmentClass(), basicItem.getName(), basicItem.getValueLong(), addToBackStack);

        } else {
            this.switchContent(basicItem.getFragmentClass(), basicItem.getName(), addToBackStack);
        }
    }

    @Override
    public void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name, final boolean addToBackStack, final Bundle bundle) {
        setOrientation();
        try {
            AbstractFragment fragment = fragmentClass.newInstance();
            if (StringUtils.hasValue(name)) {
                fragment.setName(name);
            }

            if (bundle != null) {
                fragment.setArguments(bundle);
            }

            this.switchContent(fragment, addToBackStack);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void switchContentDelay(final Class<? extends AbstractFragment> fragmentClass, int delay, final boolean animate, final boolean cleanStack, final boolean addToBackStack) {
        setOrientation();
        this.switchContentHandler.removeCallbacksAndMessages(null);
        this.switchContentHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!animate) {
                    AbstractFragment.FragmentUtils.sDisableFragmentAnimations = true;
                }
                if (cleanStack) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                switchContent(fragmentClass, addToBackStack);
                getSupportFragmentManager().executePendingTransactions();
                if (!animate) {
                    AbstractFragment.FragmentUtils.sDisableFragmentAnimations = false;
                }
            }
        }, delay);
    }

    public void switchContentDelay(final AbstractFragment fragment, int delay, final boolean animate, final boolean cleanStack, final boolean addToBackStack) {
        setOrientation();
        this.switchContentHandler.removeCallbacksAndMessages(null);
        this.switchContentHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!animate) {
                    AbstractFragment.FragmentUtils.sDisableFragmentAnimations = true;
                }
                if (cleanStack) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                switchContent(fragment, addToBackStack);
                getSupportFragmentManager().executePendingTransactions();
                if (!animate) {
                    AbstractFragment.FragmentUtils.sDisableFragmentAnimations = false;
                }
            }
        }, delay);
    }

    @Override
    public void clearFragmentStack() {
        while (this.hasBackStack()) {
            this.getSupportFragmentManager().popBackStackImmediate();
        }
    }

    @Override
    public void activityEffectTransition() {
        this.activityEffectTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void activityEffectTransition(int enterAnim, int exitAnim) {
        this.overridePendingTransition(enterAnim, exitAnim);
    }

    protected boolean hasBackStack() {
        return this.getSupportFragmentManager().getBackStackEntryCount() > 0;
    }

    @Override
    public void hideKeyboard() {
        if (this.inputManager == null) {
            this.inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        }

        if (this.getCurrentFocus() != null && this.getCurrentFocus().getWindowToken() != null) {
            this.inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void showKeyboard() {
        if (this.inputManager == null) {
            this.inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        }

        if (this.getCurrentFocus() != null && this.getCurrentFocus().getWindowToken() != null) {
            this.inputManager.showSoftInputFromInputMethod(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void executeIntent(final Intent target, @StringRes int title, @StringRes int messageProblem) {
        Intent intent = Intent.createChooser(target, this.getString(title));
        try {
            this.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, this.getString(messageProblem), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void blockOnBackPress(boolean block) {
        this.blockOnBackPress = block;
    }

    private void setOrientation()
    {
//        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
//        if (tabletSize)
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }
}

