package com.githubrepositories.model.base;

import android.os.Bundle;

public class BasicItem {

    private String name;
    private int imageId;
    private Class<? extends AbstractFragment> fragmentClass;
    private Bundle bundle;
    private String valueStr;
    private Long valueLong;

    public BasicItem() {
        super();
    }

    public BasicItem(final String name, final int imageId) {
        this.name = name;
        this.imageId = imageId;
        this.fragmentClass = null;
    }

    public BasicItem(final String name, final int imageId, final Class<? extends AbstractFragment> fragmentClass) {
        this.name = name;
        this.imageId = imageId;
        this.fragmentClass = fragmentClass;
    }

    public BasicItem(final String name, final int imageId, final Class<? extends AbstractFragment> fragmentClass, final String valueStr) {
        this(name, imageId, fragmentClass);
        this.valueStr = valueStr;
    }

    public BasicItem(final String name, final int imageId, final Class<? extends AbstractFragment> fragmentClass, final Long valueLong) {
        this(name, imageId, fragmentClass);
        this.valueLong = valueLong;
    }

    public BasicItem(final String name, final int imageId, final Class<? extends AbstractFragment> fragmentClass, final Bundle bundle) {
        this(name, imageId, fragmentClass);
        this.bundle = bundle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public Class<? extends AbstractFragment> getFragmentClass() {
        return fragmentClass;
    }

    public void setFragmentClass(Class<? extends AbstractFragment> fragmentClass) {
        this.fragmentClass = fragmentClass;
    }

    public Bundle getBundle() {
        return bundle;
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }

    public String getValueStr() {
        return valueStr;
    }

    public void setValueStr(String valueStr) {
        this.valueStr = valueStr;
    }

    public Long getValueLong() {
        return valueLong;
    }

    public void setValueLong(Long valueLong) {
        this.valueLong = valueLong;
    }
}

