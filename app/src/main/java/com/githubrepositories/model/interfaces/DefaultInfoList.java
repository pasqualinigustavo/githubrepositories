package com.githubrepositories.model.interfaces;

/**
 * Created by pasqualini on 4/16/15.
 */
public interface DefaultInfoList {
    int getIconId();
    String getIconUrl();
    String getTitle();
}