package com.githubrepositories.model.interfaces;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;

import com.githubrepositories.model.base.AbstractFragment;
import com.githubrepositories.model.base.BasicItem;

public interface ActivityBehaviour {

    void switchContent(final AbstractFragment fragment);
    void switchContent(final AbstractFragment fragment, boolean addToBackStack);
    void switchContent(final AbstractFragment fragment, int idRes, boolean animation);
    void switchContent(final AbstractFragment fragment, int idRes, boolean addToBackStack, boolean animation);
    void switchContent(final Class<? extends AbstractFragment> fragmentClass);
    void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name);
    void switchContent(final Class<? extends AbstractFragment> fragmentClass, final boolean addToBackStack);
    void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name, final boolean addToBackStack);
    void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name, final String valueStr, final boolean addToBackStack);
    void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name, final Long valueLong, final boolean addToBackStack);
    void switchContent(final Class<? extends AbstractFragment> fragmentClass, final String name, final boolean addToBackStack, final Bundle bundle);
    void switchContent(final BasicItem basicItem);
    void switchContent(final BasicItem basicItem, final boolean addToBackStack);
    void switchContentDelay(final AbstractFragment fragment, int delay, final boolean animate, final boolean cleanStack, final boolean addToBackStack);
    void switchContentDelay(final Class<? extends AbstractFragment> fragmentClass, int delay, final boolean animate, final boolean cleanStack, final boolean addToBackStack);
    void clearFragmentStack();

    void activityEffectTransition();
    void activityEffectTransition(int enterAnim, int exitAnim);

    void blockOnBackPress(final boolean block);

    void executeIntent(final Intent target, @StringRes int title, @StringRes int messageProblem);

    void hideKeyboard();
    void showKeyboard();
}

