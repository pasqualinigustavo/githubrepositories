package com.githubrepositories.fragment.adapter;

import android.view.View;

/**
 * Created by pasqualini on 15/01/16.
 */
public interface IOnItemClickListener {
    void onItemClick(View view, int position, Object item);
}
