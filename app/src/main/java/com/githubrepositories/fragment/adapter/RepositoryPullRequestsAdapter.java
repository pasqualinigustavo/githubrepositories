package com.githubrepositories.fragment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.githubrepositories.R;
import com.githubrepositories.components.imageview.LoadingImageView;
import com.githubrepositories.model.PullRequest;
import com.githubrepositories.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rafael on 15/01/16.
 */
public class RepositoryPullRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private IOnItemClickListener onItemClickListener;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private Context context = null;
    private List<PullRequest> dataset;
    private List<PullRequest> originalDataset;
    private IAdapterDataSource adapterInteractionListener = null;
    private boolean hasMoreData = true;
    private ProgressBar loadProgressBar = null;

    public RepositoryPullRequestsAdapter(Context context, List<PullRequest> list) {
        this.dataset = list;
        this.originalDataset = new ArrayList<>();
        this.hasMoreData = true;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_repository_pullrequest, parent, false);
            return new ViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_load_more, parent, false);
            return new LoadViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof LoadViewHolder) {
            if (adapterInteractionListener != null) {
                int visibility = hasMoreData ? View.VISIBLE : View.GONE;

                LoadViewHolder loadViewHolder = (LoadViewHolder) holder;
                loadViewHolder.progressBar.setVisibility(visibility);

                userWantsLoadMore(loadViewHolder.progressBar);
            }
        } else if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            final PullRequest pullRequest = dataset.get(position);

            //name
            if (!StringUtils.isNullOrEmpty(pullRequest.getTitle()))
                viewHolder.row_repository_pullrequest__textview_title.setText(pullRequest.getTitle());

            //description
            if (!StringUtils.isNullOrEmpty(pullRequest.getBody()))
                viewHolder.row_repository_pullrequest__textview_body.setText(pullRequest.getBody());

            //user region
            //image
            if (pullRequest.getUser() != null) {
                if (!StringUtils.isNullOrEmpty(pullRequest.getUser().getAvatar_url()))
                    viewHolder.row_repository_pullrequest__imageview_user.setImageUri(pullRequest.getUser().getAvatar_url());

                //nickname
                if (!StringUtils.isNullOrEmpty(pullRequest.getUser().getLogin()))
                    viewHolder.row_repository_pullrequest__textview_nickname.setText(pullRequest.getUser().getLogin());

                //name
                if (!StringUtils.isNullOrEmpty(pullRequest.getUser().getLogin()))
                    viewHolder.row_repository_pullrequest__textview_username.setText(pullRequest.getUser().getLogin());
            }
        }
    }

    @Override
    public int getItemCount() {
        if (dataset != null && dataset.size() > 0)
            return dataset.size() + 1;

        return 0;
    }

    public PullRequest getItem(int position) {
        return this.dataset.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (dataset != null) {
            if (dataset.size() > 0 && dataset.size() == position)
                return VIEW_TYPE_LOADING;
        }
        return VIEW_TYPE_ITEM;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final View row_repository_pullrequest_linearlayout_container;
        private final TextView row_repository_pullrequest__textview_title;
        private final TextView row_repository_pullrequest__textview_body;
        private LoadingImageView row_repository_pullrequest__imageview_user;
        private final TextView row_repository_pullrequest__textview_nickname;
        private final TextView row_repository_pullrequest__textview_username;

        public ViewHolder(View view) {
            super(view);
            row_repository_pullrequest_linearlayout_container = view.findViewById(R.id.row_repository_pullrequest_linearlayout_container);
            this.row_repository_pullrequest__textview_title = (TextView) view.findViewById(R.id.row_repository_pullrequest__textview_title);
            this.row_repository_pullrequest__textview_body = (TextView) view.findViewById(R.id.row_repository_pullrequest__textview_body);
            this.row_repository_pullrequest__imageview_user = (LoadingImageView) view.findViewById(R.id.row_repository_pullrequest__imageview_user);
            this.row_repository_pullrequest__textview_nickname = (TextView) view.findViewById(R.id.row_repository_pullrequest__textview_nickname);
            this.row_repository_pullrequest__textview_username = (TextView) view.findViewById(R.id.row_repository_pullrequest__textview_username);

            row_repository_pullrequest_linearlayout_container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(v, getAdapterPosition(), null);
            }
        }
    }

    static class LoadViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.cell_load_more_progress_bar);
        }
    }

    public void setOnItemClickListener(IOnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setAdapterInteractionListener(IAdapterDataSource adapterInteractionListener) {
        this.adapterInteractionListener = adapterInteractionListener;
    }

    public void setHasMoreData(boolean hasMoreData) {
        this.hasMoreData = hasMoreData;

        if (loadProgressBar != null) {
            int visibility = hasMoreData ? View.VISIBLE : View.GONE;
            loadProgressBar.setVisibility(visibility);
        }
    }

    public void setDataset(List<PullRequest> dataset) {
        this.originalDataset.clear();
        this.dataset.clear();
        if (dataset != null) {
            this.originalDataset.addAll(dataset);
            this.dataset.addAll(dataset);
        }

        notifyDataSetChanged();
    }

    public List<PullRequest> getDataset() {
        return dataset;
    }

    private void userWantsLoadMore(ProgressBar progressBar) {
        loadProgressBar = progressBar;
        if (hasMoreData && adapterInteractionListener != null) {
            loadProgressBar.setVisibility(View.VISIBLE);
            adapterInteractionListener.adapterUserWantsLoadMoreData(this);
        }
    }

    public void addMoreItemsToDateSet(List<PullRequest> items) {
        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                if (!originalDataset.contains(items.get(i)))
                    originalDataset.add(items.get(i));
            }
        }

        List<PullRequest> filtered = items;
        if (filtered != null && filtered.size() > 0) {
            for (int i = 0; i < filtered.size(); i++) {
                if (!dataset.contains(filtered.get(i)))
                    dataset.add(filtered.get(i));
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                dataset = (List<PullRequest>) results.values;
                RepositoryPullRequestsAdapter.this.notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<PullRequest> filteredResults = getFilteredResults(constraint);

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    private List<PullRequest> getFilteredResults(CharSequence constraint) {
        List<PullRequest> listResult = new ArrayList<>();
        String filter = constraint.toString();

        String name = null;
        for (PullRequest pullRequest : this.dataset) {
            name = pullRequest.getTitle();
            if (StringUtils.existInString(name, filter))
                listResult.add(pullRequest);
        }
        return listResult;
    }
}
