package com.githubrepositories.fragment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.githubrepositories.R;
import com.githubrepositories.components.imageview.LoadingImageView;
import com.githubrepositories.model.Item;
import com.githubrepositories.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rafael on 15/01/16.
 */
public class RepositoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private IOnItemClickListener onItemClickListener;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private Context context = null;
    private List<Item> dataset;
    private List<Item> originalDataset;
    private IAdapterDataSource adapterInteractionListener = null;
    private boolean hasMoreData = true;
    private ProgressBar loadProgressBar = null;

    public RepositoryAdapter(Context context, List<Item> list) {
        this.dataset = list;
        this.originalDataset = new ArrayList<>();
        this.hasMoreData = true;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_repository, parent, false);
            return new ViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_load_more, parent, false);
            return new LoadViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof LoadViewHolder) {
            if (adapterInteractionListener != null) {
                int visibility = hasMoreData ? View.VISIBLE : View.GONE;

                LoadViewHolder loadViewHolder = (LoadViewHolder) holder;
                loadViewHolder.progressBar.setVisibility(visibility);

                userWantsLoadMore(loadViewHolder.progressBar);
            }
        } else if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            final Item repository = dataset.get(position);

            //name
            if (!StringUtils.isNullOrEmpty(repository.getName()))
                viewHolder.row_repository__textview_name.setText(repository.getName());

            //description
            if (!StringUtils.isNullOrEmpty(repository.getDescription()))
                viewHolder.row_repository__textview_description.setText(repository.getDescription());

            //forks
            viewHolder.row_repository__textview_forks.setText(String.valueOf(repository.getForks_count()));

            //stars
            viewHolder.row_repository__textview_stars.setText(String.valueOf(repository.getStargazers_count()));

            //user region
            //image
            if(repository.getOwner() != null) {
                if (!StringUtils.isNullOrEmpty(repository.getOwner().getAvatar_url()))
                    viewHolder.row_repository__imageview_user.setImageUri(repository.getOwner().getAvatar_url());

                //nickname
                if (!StringUtils.isNullOrEmpty(repository.getName()))
                    viewHolder.row_repository__textview_nickname.setText(repository.getOwner().getLogin());

                //name
                if (!StringUtils.isNullOrEmpty(repository.getName()))
                    viewHolder.row_repository__textview_username.setText(repository.getOwner().getLogin());
            }
        }
    }

    @Override
    public int getItemCount() {
        if (dataset != null && dataset.size() > 0)
            return dataset.size() + 1;

        return 0;
    }

    public Item getItem(int position) {
        return this.dataset.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (dataset != null) {
            if (dataset.size() > 0 && dataset.size() == position)
                return VIEW_TYPE_LOADING;
        }
        return VIEW_TYPE_ITEM;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final View row_repository_relativelayout_container;
        private final TextView row_repository__textview_name;
        private final TextView row_repository__textview_description;
        private final TextView row_repository__textview_forks;
        private final TextView row_repository__textview_stars;
        private LoadingImageView row_repository__imageview_user;
        private final TextView row_repository__textview_nickname;
        private final TextView row_repository__textview_username;

        public ViewHolder(View view) {
            super(view);
            row_repository_relativelayout_container = view.findViewById(R.id.row_repository_relativelayout_container);
            this.row_repository__textview_name = (TextView) view.findViewById(R.id.row_repository__textview_name);
            this.row_repository__textview_description = (TextView) view.findViewById(R.id.row_repository__textview_description);
            this.row_repository__textview_forks = (TextView) view.findViewById(R.id.row_repository__textview_forks);
            this.row_repository__textview_stars = (TextView) view.findViewById(R.id.row_repository__textview_stars);
            this.row_repository__imageview_user = (LoadingImageView) view.findViewById(R.id.row_repository__imageview_user);
            this.row_repository__textview_nickname = (TextView) view.findViewById(R.id.row_repository__textview_nickname);
            this.row_repository__textview_username = (TextView) view.findViewById(R.id.row_repository__textview_username);

            row_repository_relativelayout_container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(v, getAdapterPosition(), null);
            }
        }
    }

    static class LoadViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.cell_load_more_progress_bar);
        }
    }

    public void setOnItemClickListener(IOnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setAdapterInteractionListener(IAdapterDataSource adapterInteractionListener) {
        this.adapterInteractionListener = adapterInteractionListener;
    }

    public void setHasMoreData(boolean hasMoreData) {
        this.hasMoreData = hasMoreData;

        if (loadProgressBar != null) {
            int visibility = hasMoreData ? View.VISIBLE : View.GONE;
            loadProgressBar.setVisibility(visibility);
        }
    }

    public void setDataset(List<Item> dataset) {
        this.originalDataset.clear();
        this.dataset.clear();
        if (dataset != null) {
            this.originalDataset.addAll(dataset);
            this.dataset.addAll(dataset);
        }

        notifyDataSetChanged();
    }

    public List<Item> getDataset() {
        return dataset;
    }

    private void userWantsLoadMore(ProgressBar progressBar) {
        loadProgressBar = progressBar;
        if (hasMoreData && adapterInteractionListener != null) {
            loadProgressBar.setVisibility(View.VISIBLE);
            adapterInteractionListener.adapterUserWantsLoadMoreData(this);
        }
    }

    public void addMoreItemsToDateSet(List<Item> items) {
        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                if (!originalDataset.contains(items.get(i)))
                    originalDataset.add(items.get(i));
            }
        }

        List<Item> filtered = items;
        if (filtered != null && filtered.size() > 0) {
            for (int i = 0; i < filtered.size(); i++) {
                if (!dataset.contains(filtered.get(i)))
                    dataset.add(filtered.get(i));
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                dataset = (List<Item>) results.values;
                RepositoryAdapter.this.notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Item> filteredResults = getFilteredResults(constraint);

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    private List<Item> getFilteredResults(CharSequence constraint) {
        List<Item> listResult = new ArrayList<>();
        String filter = constraint.toString();

        String name = null;
        for (Item repository : this.dataset) {
            name = repository.getName();
            if (StringUtils.existInString(name, filter))
                listResult.add(repository);
        }
        return listResult;
    }
}
