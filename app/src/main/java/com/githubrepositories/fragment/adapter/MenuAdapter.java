package com.githubrepositories.fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.githubrepositories.R;
import com.githubrepositories.model.GitHubRepositoriesMenuItem;
import com.githubrepositories.model.base.ViewHolder;

import java.util.HashMap;
import java.util.List;

public class MenuAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<GitHubRepositoriesMenuItem> list;
    private HashMap<GitHubRepositoriesMenuItem, List<GitHubRepositoriesMenuItem>> listChild;
    private boolean personalize = false;

    public MenuAdapter(final Context context, final List<GitHubRepositoriesMenuItem> list, final HashMap<GitHubRepositoriesMenuItem, List<GitHubRepositoriesMenuItem>> listChild) {
        super();
        this.context = context;
        this.list = list;
        this.listChild = listChild;
    }

    public void update(final List<GitHubRepositoriesMenuItem> list) {
        this.list = list;
        this.notifyDataSetChanged();
    }

    public void personalize(boolean personalize) {
        this.personalize = personalize;
        this.notifyDataSetChanged();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        if(groupPosition >= 0 && groupPosition < this.list.size()) {
            if(this.listChild.get(this.list.get(groupPosition)) != null && this.listChild.containsKey(this.list.get(groupPosition)))
                return this.listChild.get(this.list.get(groupPosition)).get(childPosititon);
            else
                return null;
        }
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final GitHubRepositoriesMenuItem menuItem = (GitHubRepositoriesMenuItem) getChild(groupPosition, childPosition);

        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.row_submenu, parent, false);
        }

        ImageView icon = ViewHolder.get(convertView, R.id.row_menu_icon);
        icon.setImageResource(menuItem.getImageId());

        TextView name = ViewHolder.get(convertView, R.id.row_menu_text);
        name.setText(menuItem.getName());

        TextView totalUpdate = ViewHolder.get(convertView, R.id.row_menu_total_update);
        totalUpdate.setVisibility(View.GONE);

        RelativeLayout layoutActions = ViewHolder.get(convertView, R.id.row_menu_layout_config);

        if (this.personalize) {
            layoutActions.setVisibility(View.VISIBLE);
        } else {
            layoutActions.setVisibility(View.GONE);
        }

        return convertView;

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(groupPosition >= 0 && groupPosition < this.list.size()) {
            if(this.listChild.get(this.list.get(groupPosition)) != null && this.listChild.containsKey(this.list.get(groupPosition)))
                return this.listChild.get(this.list.get(groupPosition))
                        .size();
            else
                return 0;
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.list.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.list.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        final GitHubRepositoriesMenuItem menuItem = (GitHubRepositoriesMenuItem)getGroup(groupPosition);

        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.row_menu, parent, false);
        }

        ImageView icon = ViewHolder.get(convertView, R.id.row_menu_icon);
        if(icon != null && menuItem.getImageId() > 0)
            icon.setImageResource(menuItem.getImageId());

        TextView name = ViewHolder.get(convertView, R.id.row_menu_text);
        name.setText(menuItem.getName());

        TextView totalUpdate = ViewHolder.get(convertView, R.id.row_menu_total_update);
        totalUpdate.setVisibility(View.GONE);

        RelativeLayout layoutActions = ViewHolder.get(convertView, R.id.row_menu_layout_config);

        if (this.personalize) {
            layoutActions.setVisibility(View.VISIBLE);
        } else {
            layoutActions.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
