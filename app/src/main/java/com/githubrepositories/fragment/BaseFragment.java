package com.githubrepositories.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.githubrepositories.activities.interfaces.ActivityToolbarBehaviour;
import com.githubrepositories.model.base.AbstractFragment;

/**
 * Created by pasqualini 18/06/2015.
 */
public abstract class BaseFragment extends AbstractFragment {

    private ActivityToolbarBehaviour activityToolbar;

    public BaseFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (this.activityBehaviour instanceof ActivityToolbarBehaviour) {
            this.activityToolbar = (ActivityToolbarBehaviour) this.activityBehaviour;
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return view;
    }

    protected void setToolbarTitle(String title) {
        if (this.activityToolbar != null) {
            this.activityToolbar.setToolbarTitle(title);
        }
    }

    protected void setToolbarTitle(int imageResId) {
        if (this.activityToolbar != null) {
            this.activityToolbar.setToolbarTitle(imageResId);
        }
    }

    protected void setSoftInptMode(int mode) {
        getActivity().getWindow().setSoftInputMode(mode);
    }

    @Override
    public void onStop() {
        super.onStop();
        System.gc();
    }

    protected void notificationCenter(@NonNull final String center, @NonNull final String parameter) {
        Intent intent = new Intent(center);
        intent.putExtra(parameter, true);
        LocalBroadcastManager.getInstance(this.getActivity()).sendBroadcast(intent);
    }
}
