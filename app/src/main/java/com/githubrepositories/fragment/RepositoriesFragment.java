package com.githubrepositories.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.githubrepositories.R;
import com.githubrepositories.application.AppContext;
import com.githubrepositories.fragment.adapter.IAdapterDataSource;
import com.githubrepositories.fragment.adapter.IOnItemClickListener;
import com.githubrepositories.fragment.adapter.RepositoryAdapter;
import com.githubrepositories.model.Item;
import com.githubrepositories.rest.RestAdapter;
import com.githubrepositories.rest.endpoint.SearchEndPoint;
import com.githubrepositories.rest.response.RepositoriesResponse;
import com.githubrepositories.utils.AlertUtils;
import com.githubrepositories.utils.GitHubRepositoriesConstants;
import com.githubrepositories.utils.InternetUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pasqualini on 24/06/16.
 */

public class RepositoriesFragment extends BaseFragment implements IAdapterDataSource {

    private RecyclerView fragment_repositories_listview = null;
    private SwipeRefreshLayout fragment_repositories_swiperefresh = null;
    private AppContext appContext = null;
    private RepositoryAdapter adapter = null;
    private List<Item> repositoriesList = new ArrayList<>();
    private int currentDataSetPage = 1;
    private boolean reloadData = false;

    @Override
    public void onResume() {
        super.onResume();
    }

    public RepositoriesFragment() {
        super();
        appContext = AppContext.getInstance(getActivity());
        this.layoutId = R.layout.fragment_repositories;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initComponent(View view) {
        this.setHasOptionsMenu(true);
        this.setToolbarTitle(getString(R.string.fragment_repositories_title));
        try {
            this.setSoftInptMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
            this.fragment_repositories_swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.fragment_repositories_swiperefresh);
            this.fragment_repositories_swiperefresh.setColorSchemeColors(
                    getResources().getColor(R.color.colorPrimary),
                    getResources().getColor(R.color.colorPrimaryDark));

            this.fragment_repositories_listview = (RecyclerView) view.findViewById(R.id.fragment_repositories_listview);
            this.fragment_repositories_listview.setHasFixedSize(true);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(this.getActivity(), 1);
            gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            this.fragment_repositories_listview.setLayoutManager(gridLayoutManager);
            this.fragment_repositories_listview.setItemAnimator(new DefaultItemAnimator());
        } catch (Exception e) {
            AlertUtils.showMessage(view.getContext(), getString(R.string.label_attention), e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    protected void initListeners() {
        this.fragment_repositories_swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadData = true;
                requestRepositoriesList();
                showLoadingView(true);
            }
        });
    }

    @Override
    protected void initData() {
        currentDataSetPage = 1;
        initAdapter();
        requestRepositoriesList();
    }

    private void initAdapter() {
        showLoadingView(false);
        fragment_repositories_listview.setVisibility(View.VISIBLE);
        fragment_repositories_swiperefresh.setVisibility(View.VISIBLE);
        adapter = new RepositoryAdapter(getActivity(), repositoriesList);
        adapter.setAdapterInteractionListener(this);
        fragment_repositories_listview.setAdapter(adapter);
        adapter.setOnItemClickListener(new IOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object item) {
                actionSelected(view, position, item);
            }
        });
    }

    private void actionSelected(View view, int position, Object item) {
        if (this.adapter == null) {
            return;
        }

        Item repository = this.adapter.getItem(position);
        RepositoriesFragment.this.switchContent(RepositoryDetailsFragment.newInstance(repository), true);
    }

    public void showLoadingView(final boolean show) {
        fragment_repositories_swiperefresh.post(new Runnable() {
            @Override
            public void run() {
                fragment_repositories_swiperefresh.setRefreshing(show);
            }
        });
    }

    private void requestRepositoriesList() {
        if (InternetUtils.checkNetworkConnection(getActivity()) != InternetUtils.EnumInternetType.NONE) {
            showLoadingView(true);
            new RepositoriesListDownload(getActivity()).execute();
        }
    }

    public void dataReloaded(List<Item> data, int sizeBeforeAddData) {
        currentDataSetPage = 1;
        adapter.setDataset(data);

        if (data != null) {
            adapter.setHasMoreData(true);
            if (sizeBeforeAddData < GitHubRepositoriesConstants.LIST_ITEM_ITEMS_PER_PAGE)
                adapter.setHasMoreData(false);
        }

        adapter.notifyDataSetChanged();
    }

    public void dataUpdated(List<Item> data, int sizeBeforeAddData) {
        if (data != null) {
            adapter.addMoreItemsToDateSet(data);
            if (sizeBeforeAddData < GitHubRepositoriesConstants.LIST_ITEM_ITEMS_PER_PAGE)
                adapter.setHasMoreData(false);
        } else {
            adapter.setHasMoreData(false);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void adapterUserWantsLoadMoreData(RecyclerView.Adapter apadter) {
        reloadData = false;
        currentDataSetPage++;
        requestRepositoriesList();
    }

    private class RepositoriesListDownload extends AsyncTask<Void, Void, Void> {

        private Context context = null;

        public RepositoriesListDownload(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... param) {
            readRepositories();
            return null;
        }
    }

    private void readRepositories() {
        RestAdapter.getCore()
                .create(SearchEndPoint.class)
                .getRepositories(currentDataSetPage,
                        new Callback<RepositoriesResponse>() {
                            @Override
                            public void success(final RepositoriesResponse repositories, Response response) {
                                System.out.println("SUCCESS");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        int sizeBeforeAddData = 0;
                                        List<Item> dataSource = null;
                                        if (repositories != null) {
                                            dataSource = new ArrayList<>();
                                            sizeBeforeAddData = repositories.getItems().size();
                                            dataSource.addAll(repositories.getItems());
                                            repositoriesList = repositories.getItems();
                                        }

                                        if (reloadData) {
                                            reloadData = false;
                                            dataReloaded(dataSource, sizeBeforeAddData);
                                        } else {
                                            dataUpdated(dataSource, sizeBeforeAddData);
                                        }
                                        showLoadingView(false);
                                    }
                                });
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                System.out.println("FAILURE");

                            }
                        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_home, menu);

        MenuItem menuItemSearch = menu.findItem(R.id.menu_home_overflow);
        MenuItemCompat.setActionView(menuItemSearch, R.layout.view_overflow);
        View actionViewSearch = MenuItemCompat.getActionView(menuItemSearch);
        View viewByIdSearch = actionViewSearch.findViewById(R.id.view_icon_overflow);
        viewByIdSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptions(v);
            }
        });
    }

    private void showOptions(final View view) {
        PopupMenu popupMenu = new PopupMenu(getActivity(), view);
        popupMenu.inflate(R.menu.menu_add);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.id_menu_add_example:
                        //TODO: Example of menu
                        break;
                }
                return false;
            }
        });
        popupMenu.show();
    }
}
