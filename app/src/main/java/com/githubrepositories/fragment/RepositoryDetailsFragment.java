package com.githubrepositories.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.githubrepositories.R;
import com.githubrepositories.application.AppContext;
import com.githubrepositories.fragment.adapter.IAdapterDataSource;
import com.githubrepositories.fragment.adapter.IOnItemClickListener;
import com.githubrepositories.fragment.adapter.RepositoryPullRequestsAdapter;
import com.githubrepositories.model.Item;
import com.githubrepositories.model.PullRequest;
import com.githubrepositories.model.base.AbstractFragment;
import com.githubrepositories.rest.RestAdapter;
import com.githubrepositories.rest.endpoint.SearchEndPoint;
import com.githubrepositories.utils.AlertUtils;
import com.githubrepositories.utils.GitHubRepositoriesConstants;
import com.githubrepositories.utils.GitHubRepositoriesUtils;
import com.githubrepositories.utils.InternetUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pasqualini on 24/06/16.
 */

public class RepositoryDetailsFragment extends BaseFragment implements IAdapterDataSource {

    private RecyclerView fragment_repository_details_listview = null;
    private SwipeRefreshLayout fragment_repository_details_swiperefresh = null;
    private TextView fragment_repository_details__textview_opened = null;
    private TextView fragment_repository_details__textview_closed = null;
    private AppContext appContext = null;
    private RepositoryPullRequestsAdapter adapter = null;
    private Item repository = null;
    private List<PullRequest> pullRequestList = new ArrayList<>();
    private int currentDataSetPage = 1;
    private boolean reloadData = false;

    @Override
    public void onResume() {
        super.onResume();
    }

    public RepositoryDetailsFragment() {
        super();
        appContext = AppContext.getInstance(getActivity());
        this.layoutId = R.layout.fragment_repository_details;
    }

    static AbstractFragment newInstance(Item repository) {
        RepositoryDetailsFragment fragment = new RepositoryDetailsFragment();
        fragment.repository = repository;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initComponent(View view) {
        this.setHasOptionsMenu(true);
        this.setToolbarTitle(repository.getName());

        try {
            this.setSoftInptMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
            this.fragment_repository_details__textview_opened = (TextView) view.findViewById(R.id.fragment_repository_details__textview_opened);
            this.fragment_repository_details__textview_closed = (TextView) view.findViewById(R.id.fragment_repository_details__textview_closed);
            this.fragment_repository_details_swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.fragment_repository_details_swiperefresh);
            this.fragment_repository_details_swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.fragment_repository_details_swiperefresh);
            this.fragment_repository_details_swiperefresh.setColorSchemeColors(
                    getResources().getColor(R.color.colorPrimary),
                    getResources().getColor(R.color.colorPrimaryDark));

            this.fragment_repository_details_listview = (RecyclerView) view.findViewById(R.id.fragment_repository_details_listview);
            this.fragment_repository_details_listview.setHasFixedSize(true);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(this.getActivity(), 1);
            gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            this.fragment_repository_details_listview.setLayoutManager(gridLayoutManager);
            this.fragment_repository_details_listview.setItemAnimator(new DefaultItemAnimator());
        } catch (Exception e) {
            AlertUtils.showMessage(view.getContext(), getString(R.string.label_attention), e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    protected void initListeners() {
        this.fragment_repository_details_swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestPullRequest();
                showLoadingView(true);
            }
        });
    }

    @Override
    protected void initData() {
        currentDataSetPage = 1;
        initAdapter();
        requestPullRequest();
    }

    private void initAdapter() {
        showLoadingView(false);
        fragment_repository_details_listview.setVisibility(View.VISIBLE);
        fragment_repository_details_swiperefresh.setVisibility(View.VISIBLE);

        adapter = new RepositoryPullRequestsAdapter(getActivity(), pullRequestList);
        adapter.setAdapterInteractionListener(this);
        fragment_repository_details_listview.setAdapter(adapter);
        adapter.setOnItemClickListener(new IOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object item) {
                actionSelected(view, position, item);
            }
        });

        fragment_repository_details__textview_closed.setText(String.valueOf(0));
        fragment_repository_details__textview_opened.setText(String.valueOf(0));
    }

    public void dataReloaded(List<PullRequest> data, int sizeBeforeAddData) {
        currentDataSetPage = 1;
        adapter.setDataset(data);

        if (data != null) {
            adapter.setHasMoreData(true);
            if (sizeBeforeAddData < GitHubRepositoriesConstants.LIST_ITEM_ITEMS_PER_PAGE)
                adapter.setHasMoreData(false);
        }

        adapter.notifyDataSetChanged();
    }

    public void dataUpdated(List<PullRequest> data, int sizeBeforeAddData) {
        if (data != null) {
            adapter.addMoreItemsToDateSet(data);
            if (sizeBeforeAddData < GitHubRepositoriesConstants.LIST_ITEM_ITEMS_PER_PAGE)
                adapter.setHasMoreData(false);
        } else {
            adapter.setHasMoreData(false);
            adapter.notifyDataSetChanged();
        }
    }

    private void actionSelected(View view, int position, Object item) {
        if (this.adapter == null) {
            return;
        }

        PullRequest pullRequest = this.adapter.getItem(position);
        GitHubRepositoriesUtils.openUrlWithoutConfirmation(getActivity(),pullRequest.getIssue_url());
    }

    public void showLoadingView(final boolean show) {
        fragment_repository_details_swiperefresh.post(new Runnable() {
            @Override
            public void run() {
                fragment_repository_details_swiperefresh.setRefreshing(show);
            }
        });
    }

    private void requestPullRequest() {
        if (InternetUtils.checkNetworkConnection(getActivity()) != InternetUtils.EnumInternetType.NONE) {
            showLoadingView(true);
            new PullRequestListDownload(getActivity()).execute();
        }
    }

    @Override
    public void adapterUserWantsLoadMoreData(RecyclerView.Adapter apadter) {
        reloadData = false;
        currentDataSetPage++;
        requestPullRequest();
    }

    private class PullRequestListDownload extends AsyncTask<Void, Void, Void> {

        private Context context = null;

        public PullRequestListDownload(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... param) {
            readPullRequestList();
            return null;
        }
    }

    private void readPullRequestList() {
        RestAdapter.getCore()
                .create(SearchEndPoint.class)
                .getRepositoryPullRequests(
                        repository.getOwner().getLogin(),
                        repository.getName(),
                        currentDataSetPage,
                        new Callback<List<PullRequest>>() {
                            @Override
                            public void success(final List<PullRequest> list, Response response) {
                                System.out.println("SUCCESS");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        int sizeBeforeAddData = 0;
                                        List<PullRequest> dataSource = null;
                                        if (list != null) {
                                            dataSource = new ArrayList<>();
                                            sizeBeforeAddData = list.size();
                                            dataSource.addAll(list);
                                            pullRequestList = list;
                                        }

                                        if (reloadData) {
                                            reloadData = false;
                                            dataReloaded(dataSource, sizeBeforeAddData);
                                        } else {
                                            dataUpdated(dataSource, sizeBeforeAddData);
                                        }
                                        showLoadingView(false);
                                    }
                                });
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                System.out.println("FAILURE");

                            }
                        });
    }
}
