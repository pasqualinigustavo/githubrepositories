package com.githubrepositories.fragment;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.ExpandableListView;

import com.githubrepositories.R;
import com.githubrepositories.activities.interfaces.MenuBehaviour;
import com.githubrepositories.application.AppContext;
import com.githubrepositories.fragment.adapter.MenuAdapter;
import com.githubrepositories.model.GitHubRepositoriesMenuItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Pasqualini on 24/06/16.
 */

public class GitHubRepositoriesMenu extends BaseFragment {

    private ExpandableListView listView;
    private MenuAdapter adapter;
    private HashMap<GitHubRepositoriesMenuItem, List<GitHubRepositoriesMenuItem>> listMenuGroups = null;
    private List<GitHubRepositoriesMenuItem> listMenu;
    private boolean personalize = false;
    private MenuBehaviour menuBehaviour;
    private final Handler drawerHandler = new Handler();
    private AppContext appContext = null;

    public GitHubRepositoriesMenu() {
        super();
        appContext = AppContext.getInstance(getActivity());
        this.layoutId = R.layout.menu;
    }

    @Override
    protected void initComponent(View view) {
        this.listView = (ExpandableListView) view.findViewById(R.id.menu_listView);

        if (getActivity() instanceof MenuBehaviour) {
            this.menuBehaviour = (MenuBehaviour) getActivity();
        }
    }


    @Override
    protected void initListeners() {
        this.listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (groupPosition >= 0 && groupPosition < listMenu.size()) {
                    if (listMenuGroups.get(listMenu.get(groupPosition)) == null) {
                        //abre o menu
                        actionSelectedMenu(groupPosition, -1);
                    }
                }
                return false;
            }
        });

        this.listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                if (groupPosition >= 0 && groupPosition < listMenu.size()) {
                    if (listMenuGroups.get(listMenu.get(groupPosition)) != null && listMenuGroups.containsKey(listMenu.get(groupPosition))) {
                        //abre o menu
                        actionSelectedMenu(groupPosition, childPosition);
                    }
                }
                return false;
            }
        });
    }

    @Override
    protected void initData() {
        this.listMenu = new ArrayList<>();
        this.listMenuGroups = new HashMap<>();

        //RepositoriesFragment
        this.listMenu.add(new GitHubRepositoriesMenuItem(getString(R.string.label_item_home), R.drawable.icon_menu_home, RepositoriesFragment.class));
        this.listMenuGroups.put(this.listMenu.get(this.listMenu.size() - 1), null);

        this.adapter = new MenuAdapter(this.getActivity(), this.listMenu, this.listMenuGroups);
        this.listView.setAdapter(adapter);
        expandGroups();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        //nothing
    }

    private void expandGroups() {
        for (int i = 0; i < listMenuGroups.size(); i++) {
            listView.expandGroup(i);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void actionSelectedMenu(int groupPosition, int childPosition) {
        if (this.personalize) {
            return;
        }

        if (this.menuBehaviour != null) {
            GitHubRepositoriesMenuItem menuItem = null;
            if (childPosition == -1) {
                menuItem = this.listMenu.get(groupPosition);
                if (menuItem.getFragmentClass() == null) {
//                    actionLogoff();
                    return;
                }
            } else
                menuItem = this.listMenuGroups.get(this.listMenu.get(groupPosition)).get(childPosition);
            if (menuItem != null) {
                this.menuBehaviour.openContentMenu(menuItem);
            }
        }
    }

    private void closeMenu() {
        if (this.menuBehaviour != null) {
            this.menuBehaviour.closeMenu();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


}
